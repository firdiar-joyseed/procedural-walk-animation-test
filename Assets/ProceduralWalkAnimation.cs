﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralWalkAnimation : MonoBehaviour
{
    [Header("Graphic Transform")]
    [SerializeField] Transform bodyGraphic = default;
    [SerializeField] Transform leftFootGraphic = default;
    [SerializeField] Transform rightFootGraphic = default;

    [Header("Object Transform")]
    [SerializeField] Transform bodyObject = default;
    [SerializeField] FootPart leftFootIK = default;
    [SerializeField] FootPart rightFootIK = default;


    [Header("Stepping Atribute")]
    [SerializeField] LayerMask terrainLayer = default;
    [SerializeField] MinMax steppingDistance;
    [SerializeField] MinMax movingVelocity;
    [SerializeField] float animateSpeed = 10;


    [Header("Only to show")]
    [SerializeField] float _velocity = 0;

    [SerializeField] bool isIdle = true;


    Vector3 bodyTargetPos = default;
    Vector3 leftFootTargetPos = default;
    Vector3 rightFootTargetPos = default;

    Vector3 leftOffset = default;
    Vector3 rightOffset = default;

    Vector3 prevPosVelocity = Vector3.zero;
    Vector3 prevStep = Vector3.zero;

    bool isCurrentRight = true;
    bool inMiddle = false;

    float SteppingDistance {
        get {

            float iLerp = Mathf.InverseLerp(movingVelocity.min, movingVelocity.max , Velocity);
            return Mathf.Lerp(steppingDistance.min , steppingDistance.max , iLerp);
        }
    }

    [SerializeField]
    float Velocity
    {
        get
        {
            return _velocity;
        }
        set
        {
            _velocity = value;
            isIdle = (_velocity == 0);
            if (isIdle && !inMiddle)
            {
                //bodyTargetPos.y += 0.15f;
            }
            else if(!isIdle && inMiddle)
            {
                //bodyTargetPos.y -= 0.15f;
            }
        }
    }


    [System.Serializable]
    public class MinMax {
        public float min;
        public float max;
    }

    // Start is called before the first frame update
    void Start()
    {

        leftFootIK.Initialize(leftFootGraphic, bodyGraphic, bodyObject);
        rightFootIK.Initialize(rightFootGraphic, bodyGraphic,  bodyObject);

        bodyTargetPos = bodyGraphic.localPosition;
        leftFootTargetPos = leftFootIK.transform.position;
        rightFootTargetPos = rightFootIK.transform.position;

        leftOffset = leftFootTargetPos - bodyObject.position;
        rightOffset = rightFootTargetPos - bodyObject.position;

        prevPosVelocity = bodyObject.position;
        prevStep = bodyObject.position;

    }

    // Update is called once per frame
    void Update()
    {
        Velocity = (bodyObject.position - prevPosVelocity).magnitude / Time.deltaTime;
        prevPosVelocity = bodyObject.position;

        CalculateStepDistance();
        //bodyTargetPos.y += CalculateLowestBody();
        ProceedToTarget();
    }


    void ProceedToTarget() {
        bodyGraphic.localPosition = Vector3.Lerp(bodyGraphic.localPosition, bodyTargetPos, Time.deltaTime* animateSpeed/2);
        leftFootIK.transform.position = Vector3.Lerp(leftFootIK.transform.position, leftFootTargetPos, Time.deltaTime* animateSpeed);
        rightFootIK.transform.position = Vector3.Lerp(rightFootIK.transform.position, rightFootTargetPos, Time.deltaTime* animateSpeed);
    }


    void CalculateStepDistance() 
    {
        if (!isIdle)
        {
            bodyTargetPos.y = -1.4f;
            inMiddle = false;
            float deltaMovement = (bodyObject.position - prevStep).magnitude;
            if (deltaMovement > SteppingDistance)
            {
                Step();
            }
        }
        else if (!inMiddle){
            bodyTargetPos.y = -1.25f;
            inMiddle = true;
            //rightFootTargetPos = rightFootIK.GetHit(bodyObject);
            //leftFootTargetPos = leftFootIK.GetHit(bodyObject);
        }
    }


    float CalculateLowestBody() {
        float diffHeight = 0.5f;
        float speed = 5;
        float resultAdjustment = 0;
        if (rightFootTargetPos.y < leftFootTargetPos.y)
        {
            resultAdjustment += CalculateLowestBodyDOWN(rightFootIK.GetHit(bodyObject).y - rightFootGraphic.position.y, diffHeight, speed);
            resultAdjustment += CalculateLowestBodyUP(true, diffHeight, speed);
        }
        else
        {
            resultAdjustment += CalculateLowestBodyDOWN(leftFootIK.GetHit(bodyObject).y - leftFootGraphic.position.y, diffHeight, speed);
            resultAdjustment += CalculateLowestBodyUP(true, diffHeight, speed);
        }
        return resultAdjustment;
    }

    float CalculateLowestBodyDOWN(float footToTarget , float diffHeight ,float speed ) 
    {
        if (footToTarget < -diffHeight)//if the lowest foot can't step the ground
        {
            return footToTarget * Time.deltaTime * speed;
        }
        return 0;
    }
    
    float CalculateLowestBodyUP(bool isright , float diffHeight , float speed) {

        
        
        if (isright)
        {
            
             return rightFootIK.GetMovementRoot(bodyObject, rightFootGraphic, bodyGraphic, diffHeight, speed);
        }
        else 
        {
            return leftFootIK.GetMovementRoot(bodyObject, leftFootGraphic, bodyGraphic, diffHeight, speed);
        }
    }

    void Step() {
        Debug.Log("Step");
        prevStep = bodyObject.position;
        if (isCurrentRight)
        {
            Vector3 movement =  rightFootIK.GetHit(bodyObject);
            rightFootTargetPos = movement + ( (movement - rightFootTargetPos).normalized * SteppingDistance);
        }
        else 
        {
            Vector3 movement = leftFootIK.GetHit(bodyObject);
            leftFootTargetPos = movement + ((movement - leftFootTargetPos).normalized * SteppingDistance);
        }

        isCurrentRight = !isCurrentRight;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(rightFootTargetPos, 0.4f);
        Gizmos.DrawSphere(leftFootTargetPos, 0.4f);
    }

}
