﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootPart : MonoBehaviour
{
    [SerializeField] LayerMask terrainLayer = default;

    float steppingOffset = 0;
    float heighOffset = 0;
    float deffaultHeight = 0;
    




    public void Initialize(Transform graphicFoot , Transform graphicBody , Transform body)
    {
        steppingOffset = graphicFoot.position.x - body.position.x;
        heighOffset = (graphicFoot.position.y -(body.position.y - body.localScale.y - body.GetComponent<CharacterController>().skinWidth)  );

        deffaultHeight = (graphicBody.position - graphicFoot.position).y;
    }

    // Update is called once per frame
    public Vector3 GetHit(Transform body)
    {
        Ray ray = new Ray(body.position + (body.right * steppingOffset), Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit info, 10, terrainLayer.value))
        {
            return info.point + new Vector3(0, heighOffset, 0);
        }
        else {
            return transform.position;
        }
    }

    public float GetMovementRoot(Transform body , Transform graphicFoot, Transform graphicBody , float diffrentHeight,float speed)
    {

        float footToBody = (graphicBody.position.y - graphicFoot.position.y);

        float delta = deffaultHeight - footToBody;
        if (delta > diffrentHeight)
        {
            Debug.Log("Up");
            return delta * Time.deltaTime * speed;
        }

        return 0;
    }

}
